﻿using SQLite.Net.Attributes;
using System.Runtime.Serialization;

namespace FirstIZ.DataModel
{
    [DataContract]
    class RecordedTrack
    {
        [DataMember(Order = 1)]
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        [DataMember(Order = 2)]
        public int ServerId { get; set; }
        [DataMember(Order = 3)]
        public int TractionTypeId { get; set; }
        [DataMember(Order = 4)]
        public int DrivingCapacityId { get; set; }
        [DataMember(Order = 5)]
        public string RouteStartDate { get; set; }
        [DataMember(Order = 6)]
        public string RouteStopDate { get; set; }
        public bool Uploaded{ get; set; }
        public bool Finished { get; set; }
    }
}
