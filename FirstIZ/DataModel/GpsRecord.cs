﻿using SQLite.Net.Attributes;
using System;
using System.Runtime.Serialization;

namespace FirstIZ
{
    [DataContract]
    public class GpsRecord
    {
        [DataMember(Order = 1)]
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        [DataMember(Order = 2)]
        public double Latitude { get; set; }
        [DataMember(Order = 3)]
        public double Longitude { get; set; }
        [DataMember(Order = 4)]
        public String DateRecorded { get; set; }
        [DataMember(Order = 5)]
        public int OrderNo { get; set; }
        [DataMember(Order = 6)]
        public double Accuracy { get; set; }
        public int RecordedTrackId { get; set; }

    }
}
