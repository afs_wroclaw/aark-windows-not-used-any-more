﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Windows.Devices.Radios;

namespace FirstIZ
{

    class Helpers
    {
        public static async Task<bool> IsAirplaneMode()
        {
            var radios = await Radio.GetRadiosAsync();
            bool enabled = true;
            Debug.WriteLine("Radios:");
            foreach (var radio in radios)
            {
                Debug.WriteLine("Name:" + radio.Name + radio.State);
                if (radio.State == RadioState.On)
                {
                    enabled = false;
                }
            }
            if (enabled)
            {
                //NotifyUser("Airplane Mode On");
                return true;
            }
            else
            {
                //NotifyUser("Airplane Mode Off");
                return false;
            }
            /*var temp = Windows.Devices.;
            Debug.WriteLine("networks:");
            foreach (var item in temp)
            {
                var names = item.GetNetworkNames();
                StringBuilder msg = new StringBuilder();
                
                foreach(var n in names)
                {
                    msg.Append(n + ", ");
                }
                Debug.WriteLine("networks: " + msg.ToString());
            }*/
        }
    }
}
