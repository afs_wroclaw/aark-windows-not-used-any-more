﻿using FirstIZ.DataModel;
using FirstIZ.WebInterface;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace FirstIZ
{
    class Sync
    {
        SQLite.Net.SQLiteConnection conn;
        SettingsAdapter settingsAdapter;
        HttpClient httpClient;

        public Sync()
        {
            settingsAdapter = new SettingsAdapter();
            string path = Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, "db.sqlite");
            conn = new SQLite.Net.SQLiteConnection(new SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), path);

        }

        public bool AnythingToUpload()
        {
            var query1 = conn.Table<RecordedTrack>().Where(x => x.Uploaded != true);
            var query2 = conn.Table<RecordedTrack>().Where(x => x.ServerId > 0 && x.Finished == true);
            return query1.Count() > 0 || query2.Count() > 0;
        }

        public async Task uploadStart()
        {
            var query1 = conn.Table<RecordedTrack>().Where(x => x.Uploaded != true);
            foreach (var item in query1)
            {
                await uploadStart(item);
            }
        }

        private async Task uploadStart(RecordedTrack recordedTrack)
        {
            httpClient = new HttpClient();
            try
            {
                string resourceAddress = ServerData.getWebServiceUrl() + "StartRoute";
                string authToken = settingsAdapter.getToken();

                StartRequest data = new StartRequest
                {
                    AppVersion = "Windows",
                    AuthToken = authToken,
                    DrivingCapacityID = recordedTrack.DrivingCapacityId,
                    TractionTypeID = recordedTrack.TractionTypeId,
                    RouteStartDate = recordedTrack.RouteStartDate,
                    IMEI = "UNKNOWN"
                };

                string postBody = JsonHelper.JsonSerializer(data);
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage wcfResponse = await httpClient.PostAsync(resourceAddress, new StringContent(postBody, Encoding.UTF8, "application/json"));
                await ProcessStartResult(wcfResponse, recordedTrack);
            }
            catch (HttpRequestException hre)
            {
                // NotifyUser("Error:" + hre.Message);
            }
            catch (TaskCanceledException)
            {
                // NotifyUser("Request canceled.");
            }
            catch (Exception ex)
            {
                // NotifyUser(ex.Message);
            }
            finally
            {
                //setViewState(true);
                if (httpClient != null)
                {
                    httpClient.Dispose();
                    httpClient = null;
                }
            }
        }
        private async Task ProcessStartResult(HttpResponseMessage response, RecordedTrack recordedTrack)
        {
            Debug.WriteLine("start processing stop result for id "+ recordedTrack.Id);
            string responJsonText = await response.Content.ReadAsStringAsync();
            Debug.WriteLine("result: "+ responJsonText);
            ResponseObject<StartRouteResponseData> responseObject = JsonHelper.Deserialize<ResponseObject<StartRouteResponseData>>(responJsonText);
            Debug.WriteLine("result: " + responseObject.ResponseData.NewRouteId);
            if (responseObject == null)
            {
                Debug.WriteLine("result null");
            }
            else if (responseObject.ResponseStatus)
            {
                Debug.WriteLine("processing");
                int newId = responseObject.ResponseData.NewRouteId;
                var query1 = conn.Table<RecordedTrack>().Where(x => x.Id == recordedTrack.Id);
                var dbObject = query1.First();
                dbObject.ServerId = newId;
                dbObject.Uploaded = true;
                int updated = conn.Update(dbObject);
                Debug.WriteLine("updated: " + updated);
            }
            else
                Debug.WriteLine("status false");
        }


        public async Task uploadStop()
        {
            var query1 = conn.Table<RecordedTrack>().Where(x => x.ServerId>0 && x.Finished==true);
            foreach (var item in query1)
            {
                await uploadStop(item);
            }
        }

        private async Task uploadStop(RecordedTrack recordedTrack)
        {
            httpClient = new HttpClient();
            try
            {
                string resourceAddress = ServerData.getWebServiceUrl() + "StopRoute";
                string authToken = settingsAdapter.getToken();

                Debug.WriteLine("trying to upload recorded track with id " + recordedTrack.Id);
                var queryRecords = conn.Table<GpsRecord>().Where(x => x.RecordedTrackId == recordedTrack.Id);
                IList<ObservedPoint> observedPoints = new List<ObservedPoint>();
                foreach(var point in queryRecords)
                {
                    observedPoints.Add(new ObservedPoint
                    {
                        Latitude = point.Latitude,
                        Longitude = point.Longitude,
                        DateRecorded = point.DateRecorded,
                        OrderNo = point.OrderNo,
                        Accuracy = point.Accuracy
                    });
                }

                StopRequest data = new StopRequest
                {
                    AppVersion = "Windows",
                    AuthToken = authToken,
                    RouteID = recordedTrack.ServerId,
                    RouteStopDate = recordedTrack.RouteStopDate,
                    ObservedPoints = observedPoints
                };

                string postBody = JsonHelper.JsonSerializer(data);
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage wcfResponse = await httpClient.PostAsync(resourceAddress, new StringContent(postBody, Encoding.UTF8, "application/json"));
                await ProcessStopResult(wcfResponse, recordedTrack);
            }
            catch (HttpRequestException hre)
            {
                // NotifyUser("Error:" + hre.Message);
            }
            catch (TaskCanceledException)
            {
                // NotifyUser("Request canceled.");
            }
            catch (Exception ex)
            {
                // NotifyUser(ex.Message);
            }
            finally
            {
                //setViewState(true);
                if (httpClient != null)
                {
                    httpClient.Dispose();
                    httpClient = null;
                }
            }
        }
        private async Task ProcessStopResult(HttpResponseMessage response, RecordedTrack recordedTrack)
        {
            string responJsonText = await response.Content.ReadAsStringAsync();
            Debug.WriteLine("stop route response: "+responJsonText);
            ResponseObject<StartRouteResponseData> responseObject = JsonHelper.Deserialize<ResponseObject<StartRouteResponseData>>(responJsonText);
            if (responseObject == null)
            {
            }
            else //if (responseObject.ResponseStatus) - if status is false it will be false next time - so remove it
            {
                conn.Table<GpsRecord>().Delete(x => x.RecordedTrackId == recordedTrack.Id); ;
                conn.Table<RecordedTrack>().Delete(x => x.Id == recordedTrack.Id);
            }
            //else
            //   output.Text = responseObject.ResponseDetails;
        }

    }
}
