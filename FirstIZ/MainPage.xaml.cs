﻿using FirstIZ.DataModel;
using FirstIZ.WebInterface;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Radios;
using Windows.Networking.Connectivity;
using Windows.System;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace FirstIZ
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        SQLite.Net.SQLiteConnection conn;
        SettingsAdapter settingsAdapter;
        HttpClient httpClient;
        Sync syncObject;


        public MainPage()
        {
            this.InitializeComponent();
            settingsAdapter = new SettingsAdapter();
            string path = Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, "db.sqlite");
            conn = new SQLite.Net.SQLiteConnection(new SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), path);
            conn.CreateTable<WebInterface.DownloadDataResp.DrivingCapacity>();
            conn.CreateTable<WebInterface.DownloadDataResp.TractionType>();
            conn.CreateTable<GpsRecord>();
            conn.CreateTable<RecordedTrack>();

            setDropdowns();
            syncObject = new Sync();


        }
        

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            refreshSyncButton();
            await sync();
        }

        private void refreshSyncButton()
        {
            if (syncObject.AnythingToUpload())
            {
                syncButton.Content = "SYNC !";
                syncButton.Foreground = new SolidColorBrush(Colors.Orange);
            }
            else
            {
                syncButton.Content = "sync";
                syncButton.Foreground = new SolidColorBrush(Colors.LightGray);
            }
        }

        private async Task sync()
        {
            await download();
            await syncObject.uploadStart();
            await syncObject.uploadStop();

            refreshSyncButton();
        }

        
       


        private void setDropdowns()
        {

            var query1 = conn.Table<DownloadDataResp.DrivingCapacity>().Where(x => x.IsActive == true).OrderBy(item => item.Name);
            comboBoxCapacity.ItemsSource = query1;
            var query2 = conn.Table<DownloadDataResp.TractionType>().Where(x => x.IsActive == true).OrderBy(item => item.Name);
            comboBoxTraction.ItemsSource = query2;
        }
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            if (settingsAdapter.getLogged() != true)
            {
                Frame.Navigate(typeof(LoginPage));
                return;
            }
            else if (settingsAdapter.getRecording())
            {
                Frame.Navigate(typeof(RecordingPage));
                return;
            }
            headerText.Text = settingsAdapter.getUserName();
        }




        private async void button_Click(object sender, RoutedEventArgs e)
        {
            WebInterface.DownloadDataResp.TractionType type = (WebInterface.DownloadDataResp.TractionType)comboBoxTraction.SelectedItem;
            WebInterface.DownloadDataResp.DrivingCapacity capacity = (WebInterface.DownloadDataResp.DrivingCapacity)comboBoxCapacity.SelectedItem;
            if (type == null || capacity == null)
            {
                var messageDialog = new Windows.UI.Popups.MessageDialog("Please complete");
                await messageDialog.ShowAsync();
            }
            else
            {
                //string message = "selected ids " + type.Id + " ," + capacity.Id;
                var messageDialog = new Windows.UI.Popups.MessageDialog("Start AARK session?");
                messageDialog.Commands.Add(new Windows.UI.Popups.UICommand("Cancel") { Id = 0 });
                messageDialog.Commands.Add(new Windows.UI.Popups.UICommand("OK") { Id = 1 });
                var result = await messageDialog.ShowAsync();
                if ((int)result.Id == 1)
                {
                    RecordedTrack dataToInsert = new RecordedTrack { TractionTypeId = type.Id, DrivingCapacityId = capacity.Id, RouteStartDate = DateHelper.getDateString() };
                    int status = conn.Insert(dataToInsert);
                    int id = dataToInsert.Id; //Acording to the documentation for the SQLIte component, the Insert method updates the id by reference
                    settingsAdapter.setRecording(true);
                    settingsAdapter.setActiveRouteId(id);
                    
                    Frame.Navigate(typeof(RecordingPage));
                }
                else
                {

                }
                   
            }
        }

        private async void logout_Click(object sender, RoutedEventArgs e)
        {
            if (settingsAdapter.getRecording())
            {
                var messageDialog = new Windows.UI.Popups.MessageDialog("Cannot logout when recording");
                messageDialog.Commands.Add(new Windows.UI.Popups.UICommand("OK") { Id = 0 });
                messageDialog.CancelCommandIndex = 0;
                await messageDialog.ShowAsync();
                return;
            }
            else {
                var messageDialog = new Windows.UI.Popups.MessageDialog("Do you want to logout");
                messageDialog.Commands.Add(new Windows.UI.Popups.UICommand("No") { Id = 0 });
                messageDialog.Commands.Add(new Windows.UI.Popups.UICommand("Yes") { Id = 1 });
                messageDialog.DefaultCommandIndex = 1;
                messageDialog.CancelCommandIndex = 0;
                var result = await messageDialog.ShowAsync();
                if ((int)result.Id == 1)
                {
                    settingsAdapter.setLogged(false);
                    Frame.Navigate(typeof(LoginPage));
                }
            }
        }

        private async Task download()
        {
            httpClient = new HttpClient();
            try
            {
                string resourceAddress = ServerData.getWebServiceUrl() + "DownloadData";
                string authToken = settingsAdapter.getToken();
                string lastSync = settingsAdapter.getLastSync();

                DownloadData data = new DownloadData { AppVersion = "Windows", AuthToken = authToken, LastSyncDate = lastSync };
                string postBody = JsonHelper.JsonSerializer(data);
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage wcfResponse = await httpClient.PostAsync(resourceAddress, new StringContent(postBody, Encoding.UTF8, "application/json"));
                await ProcessResult(wcfResponse);
            }
            catch (HttpRequestException hre)
            {
               // NotifyUser("Error:" + hre.Message);
            }
            catch (TaskCanceledException)
            {
                NotifyUser("Request canceled.");
            }
            catch (Exception ex)
            {
              //  NotifyUser(ex.Message);
            }
            finally
            {
                //setViewState(true);
                if (httpClient != null)
                {
                    httpClient.Dispose();
                    httpClient = null;
                }
            }
        }
        private async Task ProcessResult(HttpResponseMessage response)
        {
            string responJsonText = await response.Content.ReadAsStringAsync();
            ResponseObject<DownloadDataResp> responseObject = JsonHelper.Deserialize<ResponseObject<DownloadDataResp>>(responJsonText);
            if (responseObject == null) {
                NotifyUser("not able to sync now");
            }
            else if (responseObject.ResponseStatus)
            {
                IList<DownloadDataResp.DrivingCapacity> capacites = responseObject.ResponseData.DrivingCapacities;
                conn.InsertAll(capacites);
                conn.UpdateAll(capacites);
                IList<DownloadDataResp.TractionType> types = responseObject.ResponseData.TractionTypes;
                conn.InsertAll(types);
                conn.UpdateAll(types);
                setDropdowns();

            }
            else
               NotifyUser("not able to sync now " +responseObject.ResponseDetails);
        }


     




        public void NotifyUser(string message)
        {
            this.OutputField.Text = message;
        }

        private async void sync_Click(object sender, RoutedEventArgs e)
        {
            await sync();
        }
    }
}
