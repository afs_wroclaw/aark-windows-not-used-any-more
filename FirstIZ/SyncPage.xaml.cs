﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using System.Runtime.Serialization.Json;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace FirstIZ
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SyncPage : Page
    {
        HttpClient httpClient;
        public SyncPage()
        {
            this.InitializeComponent();
        }
        
        private async void Start_Click(object sender, RoutedEventArgs e)
        {
            // Clear text of Output textbox 
            this.OutputField.Text = string.Empty;
            this.StatusBlock.Text = string.Empty;

            this.StartButton.IsEnabled = false;
            httpClient = new HttpClient();
            try
            {
                string resourceAddress = "http://requestb.in/xko6tpxk";
                int age = Convert.ToInt32(this.Agetxt.Text);
                if (age > 120 || age < 0)
                {
                    throw new Exception("Age must be between 0 and 120");
                }
                Person p = new Person { Name = this.Nametxt.Text, Age = age };
                string postBody = JsonSerializer(p);
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage wcfResponse = await httpClient.PostAsync(resourceAddress, new StringContent(postBody, Encoding.UTF8, "application/json"));
                await DisplayTextResult(wcfResponse, OutputField);
            }
            catch (HttpRequestException hre)
            {
                NotifyUser("Error:" + hre.Message);
            }
            catch (TaskCanceledException)
            {
                NotifyUser("Request canceled.");
            }
            catch (Exception ex)
            {
                NotifyUser(ex.Message);
            }
            finally
            {
                this.StartButton.IsEnabled = true;
                if (httpClient != null)
                {
                    httpClient.Dispose();
                    httpClient = null;
                }
            }
        }

        /// <summary>
        /// Display Result which returns from WCF Service in "OutputField" Textbox
        /// </summary>
        /// <param name="response">Http response Message</param>
        /// <param name="output">Show result control</param>
        /// <returns></returns>
        private async Task DisplayTextResult(HttpResponseMessage response, TextBox output)
        {
            string responJsonText = await response.Content.ReadAsStringAsync();
            GetJsonValue(responJsonText);
            output.Text += GetJsonValue(responJsonText);
        }

        /// <summary>
        /// Serialize Person object to Json string
        /// </summary>
        /// <param name="objectToSerialize">Person object instance</param>
        /// <returns>return Json String</returns>
        public static string JsonSerializer(Person objectToSerialize)
        {
            if (objectToSerialize == null)
            {
                throw new ArgumentException("objectToSerialize must not be null");
            }
            MemoryStream ms = null;

            DataContractJsonSerializer serializer = new DataContractJsonSerializer(objectToSerialize.GetType());
            ms = new MemoryStream();
            serializer.WriteObject(ms, objectToSerialize);
            ms.Seek(0, SeekOrigin.Begin);
            StreamReader sr = new StreamReader(ms);
            return sr.ReadToEnd();
        }

        /// <summary>
        /// Get Result from Json String
        /// </summary>
        /// <param name="jsonString">Json string which returns from WCF Service</param>
        /// <returns>Result string</returns>
        public static string GetJsonValue(string jsonString)
        {
            int ValueLength = jsonString.LastIndexOf("\"") - (jsonString.IndexOf(":") + 2);
            string value = jsonString.Substring(jsonString.IndexOf(":") + 2, ValueLength);
            return value;

        }

        public void NotifyUser(string message)
        {
            this.StatusBlock.Text = message;
        }




    }
}
