﻿using System.Runtime.Serialization;

namespace FirstIZ
{
    [DataContract]
    public class Person
    {
        [DataMember(Order = 1)]
        public string Name { get; set; }
        [DataMember(Order = 2)]
        public int Age { get; set; }
    }
}
