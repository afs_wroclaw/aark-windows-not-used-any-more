﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace FirstIZ
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPageCopy : Page
    {
        SQLite.Net.SQLiteConnection conn;
        SettingsAdapter settingsAdapter;


        public MainPageCopy()
        {
            this.InitializeComponent();
            settingsAdapter = new SettingsAdapter();
           // settingsAdapter.setLogged(false);
            string path = Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, "db.sqlite");
            conn = new SQLite.Net.SQLiteConnection(new SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), path);
            conn.CreateTable<GpsRecord>();
            
        }
        

        private void Add_Click(object sender, RoutedEventArgs e)
        {
           // var add = conn.Insert(new GpsRecord() { content = textBox.Text });
        }

        private void Show_Click(object sender, RoutedEventArgs e)
        {
            var query = conn.Table<GpsRecord>().Where(x => x.Id > 1);
            System.Text.StringBuilder builder = new System.Text.StringBuilder();
            foreach (var item in query)
            {
               // builder.Append(String.Format("{0} : {1}\n", item.id, item.content));
            }
            resultBox.Text = builder.ToString();
        }

        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            conn.Table<GpsRecord>().Delete(x => x.Id > 0);
        }

        public void insertGpsRecord(GpsRecord item)
        {
            var add = conn.Insert(item);
        }
        public void clearGpsRecord()
        {
            conn.Table<GpsRecord>().Delete(x => x.Id > 0);
        }
        public SQLite.Net.TableQuery<GpsRecord> getAllRecords()
        {
            return conn.Table<GpsRecord>().Where(x => x.Id > 1);
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            
            this.Frame.Navigate(typeof(SyncPage));
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            if (settingsAdapter.getLogged() != true)
            {
                Frame.Navigate(typeof(LoginPage));
            }
        }
    }
}
