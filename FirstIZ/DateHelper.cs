﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstIZ
{
    class DateHelper
    {

        public static String getDateString()
        {
            return DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");
        }

    }
}
