﻿namespace FirstIZ
{
    class ServerData
    {
        public static string WEB_SERVICE_URL_LIVE = "http://infinitezero.service.afsgo.com/AppService.svc/";
        public static string WEB_SERVICE_URL_DEV = "http://infinitezero.service.afsdevs.com/AppService.svc/";
        public static string NAME_LIVE = "live";
        public static string NAME_DEV = "developer";

        private static SettingsAdapter settingsAdapter = new SettingsAdapter();

        public static string getWebServiceUrl()
        {
            string url = settingsAdapter.getServerUrl();
            if (url == null)
            {
                settingsAdapter.setServerUrl(WEB_SERVICE_URL_LIVE);
                url = settingsAdapter.getServerUrl();
            }
            return url;
        }
        public static void makeItDev()
        {
            settingsAdapter.setServerUrl(WEB_SERVICE_URL_DEV);
        }
        public static void makeItLive()
        {
            settingsAdapter.setServerUrl(WEB_SERVICE_URL_LIVE);
        }
        public static void toggle()
        {
            string url = getWebServiceUrl();
            if (url.Contains("afsgo"))
                settingsAdapter.setServerUrl(WEB_SERVICE_URL_DEV);
            else if (url.Contains("afsdev"))
                settingsAdapter.setServerUrl(WEB_SERVICE_URL_LIVE);
            else
                settingsAdapter.setServerUrl(WEB_SERVICE_URL_LIVE);
        }

        public static string getWebServiceName()
        {
            string url = getWebServiceUrl();
            if (url.Contains("afsgo"))
                return NAME_LIVE;
            else if (url.Contains("afsdev"))
                return NAME_DEV;
            else
                return url;
        }
    }
}
