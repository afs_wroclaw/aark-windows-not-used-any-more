﻿using FirstIZ.DataModel;
using FirstIZ.WebInterface;
using System;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Background;
using Windows.ApplicationModel.ExtendedExecution;
using Windows.Data.Xml.Dom;
using Windows.Devices.Geolocation;
using Windows.Devices.Radios;
using Windows.System;
using Windows.UI.Core;
using Windows.UI.Notifications;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace FirstIZ
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class RecordingPage : Page
    {

        SQLite.Net.SQLiteConnection conn;
        SettingsAdapter settingsAdapter;
        HttpClient httpClient;
        int recordedTrackId;
        private bool isActive = true;

        private ExtendedExecutionSession session = null;

        private static RecordingPage _rootPage;

        private Geolocator _geolocator = null;

        public RecordingPage()
        {
            Debug.WriteLine("Constructor");
            this.InitializeComponent();
            settingsAdapter = new SettingsAdapter();
            string path = Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, "db.sqlite");
            conn = new SQLite.Net.SQLiteConnection(new SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), path);
            recordedTrackId = settingsAdapter.getActiveRouteId();
            //Debug.WriteLine("screen for route " + recordedTrackId);

            var query1 = conn.Table<RecordedTrack>().Where(x => x.Id == recordedTrackId);
            var dbObject = query1.First();
            var queryCapacity = conn.Table<DownloadDataResp.DrivingCapacity>().Where(x => x.Id == dbObject.DrivingCapacityId);
            var dbObjectCapacity = queryCapacity.First();
            var queryType = conn.Table<DownloadDataResp.TractionType>().Where(x => x.Id == dbObject.TractionTypeId);
            var dbObjectType = queryType.First();

            driver_text.Text = settingsAdapter.getUserName();
            capacity_text.Text = dbObjectCapacity.Name;
            traction_text.Text = dbObjectType.Name;
            routeServerId.Text = dbObject.ServerId.ToString() + "(" + recordedTrackId + ")";

            Application.Current.Suspending += new SuspendingEventHandler(OnSuspending);
            Application.Current.Resuming += OnResuming;
            //Application.Current.Suspending += OnPausing;

            _rootPage = this;
            StartTracking();
            Sync sync = new Sync();
            sync.uploadStart();
            showToastSimple("constructor", "called");
        }

        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            // End the Extended Execution Session.
            // Only one extended execution session can be held by an application at one time.
            Debug.WriteLine("On Navigated To");

            bool isEnabled = await IsAirplaneMode();
            if (!isEnabled)
            {
                var airplaneDialog = new Windows.UI.Popups.MessageDialog("Do you want to turn on flight mode");
                airplaneDialog.Commands.Add(new Windows.UI.Popups.UICommand("No") { Id = 0 });
                airplaneDialog.Commands.Add(new Windows.UI.Popups.UICommand("Yes") { Id = 1 });
                var airplaneResult = await airplaneDialog.ShowAsync();
                if ((int)airplaneResult.Id == 1)
                {
                    await Launcher.LaunchUriAsync(new Uri("ms-settings:network-airplanemode"));
                }
                else
                {

                }
            }

        }

        private async void OnPausing(Object sender, Object e)
        {
            Debug.WriteLine("On Pausing");
            
            await startLocationExtensionSession();
        }

        private async void OnResuming(Object sender, Object e)
        {
            //this.OutputField.Text += "On Resume";
            Debug.WriteLine("On Resuming");
            showToastSimple("on resume", "called");

            await startLocationExtensionSession();
        }

        private async void OnSuspending(object sender, SuspendingEventArgs e)
        {
            Debug.WriteLine("On Suspending");
            await startLocationExtensionSession();
        }

        private async Task startLocationExtensionSession()
        {
            if (!isActive)
                return;

            ClearExtendedExecution();
            // var deferral = e.SuspendingOperation.GetDeferral();
            session = new ExtendedExecutionSession();
            session.Description = "Location Tracker";
            session.Reason = ExtendedExecutionReason.LocationTracking;
            session.Revoked += SessionRevoked;
            var result = await session.RequestExtensionAsync();
            if (result == ExtendedExecutionResult.Denied)
            {
                Debug.WriteLine("ERROR: denied to run session ");
                showBackgroundTaskError();
            }
            else { 
                Debug.WriteLine("Started Session");
            }
        }


        void ClearExtendedExecution()
        {
            if (session != null)
            {
                session.Revoked -= SessionRevoked;
                session.Dispose();
                session = null;
            }
        }


        private async void button_Click(object sender, RoutedEventArgs e)
        {
            StopTracking();
            isActive = false;
            ClearExtendedExecution();
            settingsAdapter.setRecording(false);
            var query1 = conn.Table<RecordedTrack>().Where(x => x.Id == recordedTrackId);
            var dbObject = query1.First();
            dbObject.Finished = true;
            dbObject.RouteStopDate = DateHelper.getDateString();
            int updated = conn.Update(dbObject);

            bool isEnabled = await Helpers.IsAirplaneMode();
            if (isEnabled)
            {
                var airplaneDialog = new Windows.UI.Popups.MessageDialog("Do you want to disable flight mode");
                airplaneDialog.Commands.Add(new Windows.UI.Popups.UICommand("No") { Id = 0 });
                airplaneDialog.Commands.Add(new Windows.UI.Popups.UICommand("Yes") { Id = 1 });
                var airplaneResult = await airplaneDialog.ShowAsync();
                if ((int)airplaneResult.Id == 1)
                {
                    await Launcher.LaunchUriAsync(new Uri("ms-settings:network-airplanemode"));
                }
                else
                {

                }
            }

            Frame.Navigate(typeof(MainPage));
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(TrackPosition));
        }

        //////////////////////////
        private async void StartTracking()
        {
            // Request permission to access location
            var accessStatus = await Geolocator.RequestAccessAsync();

            await startLocationExtensionSession();

            switch (accessStatus)
            {
                case GeolocationAccessStatus.Allowed:
                    _geolocator = new Geolocator { ReportInterval = 2000 };

                    // Subscribe to PositionChanged event to get updated tracking positions
                    _geolocator.PositionChanged += OnPositionChanged;
                    _geolocator.DesiredAccuracy = PositionAccuracy.High;

                    // Subscribe to StatusChanged event to get updates of location status changes
                    _geolocator.StatusChanged += OnStatusChanged;
                    _rootPage.NotifyUser("Waiting for update...", null);
                    break;
                case GeolocationAccessStatus.Denied:
                    _rootPage.NotifyUser("Access to location is denied.", null);
                    break;
                case GeolocationAccessStatus.Unspecified:
                    _rootPage.NotifyUser("Unspecificed error!", null);
                    break;
            }
        }

        private void StopTracking()
        {
            _geolocator.PositionChanged -= OnPositionChanged;
            _geolocator.StatusChanged -= OnStatusChanged;
            _geolocator = null;


            // Clear status
            _rootPage.NotifyUser("", null);
        }

        async private void OnPositionChanged(Geolocator sender, PositionChangedEventArgs e)
        {
            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                SaveLocationData(e.Position);
                _rootPage.NotifyUser("", e.Position);
            });
        }

        async private void OnStatusChanged(Geolocator sender, StatusChangedEventArgs e)
        {
            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {

                switch (e.Status)
                {
                    case PositionStatus.Ready:
                        // Location platform is providing valid data.
                        ScenarioOutput_Status.Text = "Ready";
                        _rootPage.NotifyUser("Location platform is ready.", null);
                        break;

                    case PositionStatus.Initializing:
                        // Location platform is attempting to acquire a fix. 
                        ScenarioOutput_Status.Text = "Initializing";
                        _rootPage.NotifyUser("Location platform is attempting to obtain a position.", null);
                        break;

                    case PositionStatus.NoData:
                        // Location platform could not obtain location data.
                        ScenarioOutput_Status.Text = "No data";
                        _rootPage.NotifyUser("Not able to determine the location.", null);
                        break;

                    case PositionStatus.Disabled:
                        // The permission to access location data is denied by the user or other policies.
                        ScenarioOutput_Status.Text = "Disabled";
                        _rootPage.NotifyUser("Access to location is denied.", null);


                        break;

                    case PositionStatus.NotInitialized:
                        // The location platform is not initialized. This indicates that the application 
                        // has not made a request for location data.
                        ScenarioOutput_Status.Text = "Not initialized";
                        _rootPage.NotifyUser("No request for location is made yet.", null);
                        break;

                    case PositionStatus.NotAvailable:
                        // The location platform is not available on this version of the OS.
                        ScenarioOutput_Status.Text = "Not available";
                        _rootPage.NotifyUser("Location is not available on this version of the OS.", null);
                        break;

                    default:
                        ScenarioOutput_Status.Text = "Unknown";
                        _rootPage.NotifyUser(string.Empty, null);
                        break;
                }
            });
        }

        public void NotifyUser(string message, Geoposition position)
        {
            if (_rootPage != null)
            {
                //this.OutputField.Text = message;
                UpdateLocationData(position);
            }
        }
        private void UpdateLocationData(Geoposition position)
        {
            if (position == null)
            {
                ScenarioOutput_Latitude.Text = "No data";
                ScenarioOutput_Longitude.Text = "No data";
                ScenarioOutput_Accuracy.Text = "No data";
                ScenarioOutput_Points.Text = "-";
            }
            else
            {
                ScenarioOutput_Latitude.Text = position.Coordinate.Point.Position.Latitude.ToString();
                ScenarioOutput_Longitude.Text = position.Coordinate.Point.Position.Longitude.ToString();
                ScenarioOutput_Accuracy.Text = position.Coordinate.Accuracy.ToString();
                ScenarioOutput_Points.Text = orderCounter.ToString();
            }
        }

        private static int orderCounter = 1;
        private void SaveLocationData(Geoposition position)
        {
            Debug.WriteLine("inserting gps record for " + recordedTrackId);
            var records = getAllRecords(recordedTrackId);
            orderCounter = records.Count()  + 1;
            if (position.Coordinate.Accuracy < 1000)
                conn.Insert(new GpsRecord
                {
                    RecordedTrackId = recordedTrackId,
                    Latitude = position.Coordinate.Point.Position.Latitude,
                    Longitude = position.Coordinate.Point.Position.Longitude,
                    DateRecorded = DateHelper.getDateString(),
                    OrderNo = orderCounter,
                    Accuracy = position.Coordinate.Accuracy
                });
        }

        //////////////////////////


        public void insertGpsRecord(GpsRecord item)
        {
            var add = conn.Insert(item);
        }
        public void clearGpsRecord(int recordedTrackId)
        {
            conn.Table<GpsRecord>().Delete(x => x.RecordedTrackId == recordedTrackId);
        }
        public SQLite.Net.TableQuery<GpsRecord> getAllRecords(int recordedTrackId)
        {
            return conn.Table<GpsRecord>().Where(x => x.RecordedTrackId == recordedTrackId);
        }

        private void testBackground_Click(object sender, RoutedEventArgs e)
        {
            showBackgroundTaskError();
            /*if (checkTask())
            {
                Debug.WriteLine("taks already registered");
            }
            else
            {
                registerTask();
            }*/
        }

        private bool checkTask()
        {
            var taskRegistered = false;
            var exampleTaskName = "ExampleBackgroundTask";

            foreach (var task in BackgroundTaskRegistration.AllTasks)
            {
                if (task.Value.Name == exampleTaskName)
                {
                    task.Value.Unregister(true);
                    //taskRegistered = true;
                    //break;
                }
            }
            return taskRegistered;
        }
        private async void registerTask()
        {
            Debug.WriteLine("clicked ");
            var builder = new BackgroundTaskBuilder();
            var trigger = new ApplicationTrigger();
            builder.Name = "ExampleBackgroundTask";
            builder.TaskEntryPoint = "RuntimeComponent1.ExampleBackgroundTask";
            builder.SetTrigger(trigger);
            BackgroundTaskRegistration task = builder.Register();
            task.Progress += new BackgroundTaskProgressEventHandler(OnProgress);

            var result = await trigger.RequestAsync();
            Debug.WriteLine("requested " + result.ToString());
        }

        /// <summary>
        /// Handle background task progress.
        /// </summary>
        /// <param name="task">The task that is reporting progress.</param>
        /// <param name="e">Arguments of the progress report.</param>
        private void OnProgress(IBackgroundTaskRegistration task, BackgroundTaskProgressEventArgs args)
        {
            var ignored = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                var progress = "Progress: " + args.Progress + "%";
                Debug.WriteLine(progress);
                NotifyUser(progress, null);
            });
        }
        //////////


        private async void SessionRevoked(object sender, ExtendedExecutionRevokedEventArgs args)
        {
            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                switch (args.Reason)
                {
                    case ExtendedExecutionRevokedReason.Resumed:
                        Debug.WriteLine("Extended execution revoked due to returning to foreground.");
                        break;

                    case ExtendedExecutionRevokedReason.SystemPolicy:
                        Debug.WriteLine("Extended execution revoked due to system policy.");
                        showBackgroundTaskError("(system policy)");
                        break;
                }
            });
            await startLocationExtensionSession();
        }

        public async Task<bool> IsAirplaneMode()
        {
            var radios = await Radio.GetRadiosAsync();
            bool enabled = true;
            Debug.WriteLine("Radios:");
            foreach (var radio in radios)
            {
                Debug.WriteLine("Name:" + radio.Name + radio.State);
                if (radio.State == RadioState.On)
                {
                    enabled = false;
                }
            }
            if (enabled)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void showBackgroundTaskError(string text="")
        {
#if DEBUG
            showToast("Error", "Cannot run the app in background. "+text, true);
#endif
        }

        private void showBackgroundTaskSuccess()
        {
            //showToast("App running", "Cannot run tracking in background", false);
        }

        private void showToast(string title, string message, bool sound) {
            
            string xml = @"<toast launch=""fromError"">
            <visual>
            <binding template=""ToastGeneric"">
                <text>" + title + @"</text>
                <text>" + message+ @"</text>
            </binding>
            </visual>"+
            (sound? @"<audio src=""ms-winsoundevent:Notification.Looping.Alarm6""/>" : "")
          + @" <actions><action activationType=""foreground"" content=""Open the app !"" arguments=""fromError""/> </actions>
            </toast>";

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            ToastNotification toast = new ToastNotification(doc);
            ToastNotificationManager.CreateToastNotifier().Show(toast);
        }

        private void showToastSimple(string title, string message)
        {
            return;
            //string xml = @"<toast>
            //<visual>
            //<binding template=""ToastGeneric"">
            //    <text>" + title + @"</text>
            //    <text>" + message + @"</text>
            //</binding>
            //</visual>
            //</toast>";

            //XmlDocument doc = new XmlDocument();
            //doc.LoadXml(xml);

            //ToastNotification toast = new ToastNotification(doc);
            //ToastNotificationManager.CreateToastNotifier().Show(toast);
        }

        private void resetTask(object sender, RoutedEventArgs e)
        {
            startLocationExtensionSession();
        }
    }
}
