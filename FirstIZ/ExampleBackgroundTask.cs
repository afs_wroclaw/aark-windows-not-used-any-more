﻿using Windows.ApplicationModel.Background;
using System;
using Windows.Devices.Geolocation;
using Windows.UI.Core;
using System.Threading.Tasks;
using FirstIZ;
using System.Diagnostics;

namespace Tasks
{
    public sealed class ExampleBackgroundTaskLOC : IBackgroundTask
    {
        private Geolocator _geolocator = null;
        BackgroundTaskDeferral _deferral = null;
        private IBackgroundTaskInstance _taskInstance;
        SQLite.Net.SQLiteConnection conn;
        SettingsAdapter settingsAdapter;
        private int routeId;

        public async void Run(IBackgroundTaskInstance taskInstance)
        {
            //
            // TODO: Insert code to start one or more asynchronous methods using the
            //       await keyword, for example:
            //
            // await ExampleMethodAsync();
            //
            _deferral = taskInstance.GetDeferral();
            _taskInstance = taskInstance;
            _taskInstance.Canceled += OnCanceled;
           // settingsAdapter = new SettingsAdapter();
           // routeId = settingsAdapter.getActiveRouteId();
          //  Debug.WriteLine("on Run method - routeId "+routeId);



            taskInstance.Progress = 1;
       //    await StartTracking();
            //_deferral.Complete();
        }

        private void OnCanceled(IBackgroundTaskInstance sender, BackgroundTaskCancellationReason reason)
        {

            StopTracking();
            Debug.WriteLine("Background " + sender.Task.Name + " Cancel Requested...");
        }


        private async Task StartTracking()
        {
            // Request permission to access location
            
            var accessStatus = await Geolocator.RequestAccessAsync();

            switch (accessStatus)
            {
                case GeolocationAccessStatus.Allowed:
                    _geolocator = new Geolocator { ReportInterval = 5000 };

                    // Subscribe to PositionChanged event to get updated tracking positions
                    _geolocator.PositionChanged += OnPositionChanged;
                    _geolocator.DesiredAccuracy = PositionAccuracy.High;

                    // Subscribe to StatusChanged event to get updates of location status changes
                   // _geolocator.StatusChanged += OnStatusChanged;

                    Debug.WriteLine("Waiting for update...");
                    break;

                case GeolocationAccessStatus.Denied:
                    Debug.WriteLine("Access to location is denied.");
                    break;

                case GeolocationAccessStatus.Unspecified:
                    Debug.WriteLine("Unspecificed error!");
                    break;
            }
        }

        private void StopTracking()
        {
            _geolocator.PositionChanged -= OnPositionChanged;
           // _geolocator.StatusChanged -= OnStatusChanged;
            _geolocator = null;


            // Clear status
            //_rootPage.NotifyUser("", null);
        }

        async private void OnPositionChanged(Geolocator sender, PositionChangedEventArgs e)
        {
            _taskInstance.Progress += 1;
            
        }

        private static int orderCounter = 1;
        private void SaveLocationData(Geoposition position)
        {
            Debug.WriteLine("inserting gps record for " + routeId);
            //var records = getAllRecords(routeId);
            //orderCounter = records.Count();
            if (position.Coordinate.Accuracy < 150)
                conn.Insert(new GpsRecord
                {
                    RecordedTrackId = routeId,
                    Latitude = position.Coordinate.Point.Position.Latitude,
                    Longitude = position.Coordinate.Point.Position.Longitude,
                    DateRecorded = DateHelper.getDateString(),
                    OrderNo = ++orderCounter
                });
        }
    }
}