﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstIZ
{
    class SettingsAdapter
    {


        private Windows.Storage.ApplicationDataContainer localSettings;
        public SettingsAdapter()
        {
            localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
        }


        private readonly static String IS_LOGGED = "IS_LOGGED";
        public Boolean getLogged()
        {
            try
            {
                return (Boolean)localSettings.Values[IS_LOGGED];
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public void setLogged(Boolean value)
        {
            localSettings.Values[IS_LOGGED] = value;
        }
        private readonly static String USER_NAME = "USER_NAME";
        public String getUserName()
        {
            return (String)localSettings.Values[USER_NAME];
        }
        public void setUserName(String value)
        {
            localSettings.Values[USER_NAME] = value;
        }
        private readonly static String USER_TOKEN = "USER_TOKEN";
        public String getToken()
        {
            return (String)localSettings.Values[USER_TOKEN];
        }
        public void setToken(String value)
        {
            localSettings.Values[USER_TOKEN] = value;
        }
        private readonly static String IS_RECORDING = "IS_RECORDING";
        public Boolean getRecording()
        {
            try
            {
                return (Boolean)localSettings.Values[IS_RECORDING];
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public void setRecording(Boolean value)
        {
            localSettings.Values[IS_RECORDING] = value;
        }


        private readonly static String LAST_SYNC = "LAST_SYNC";
        public String getLastSync()
        {
            try
            {
                return (String)localSettings.Values[LAST_SYNC];
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public void setLastSync(String value)
        {
            localSettings.Values[LAST_SYNC] = value;
        }


        private readonly static String ACTIVE_ROUTE_ID = "ACTIVE_ROUTE_ID";
        public int getActiveRouteId()
        {
            try
            {
                return (int)localSettings.Values[ACTIVE_ROUTE_ID];
            }
            catch (Exception e)
            {
                return -1;
            }
        }
        public void setActiveRouteId(int value)
        {
            localSettings.Values[ACTIVE_ROUTE_ID] = value;
        }

        private readonly static String SERVER_URL = "SERVER_URL";
        public String getServerUrl()
        {
            try
            {
                return (String)localSettings.Values[SERVER_URL];
            }
            catch
            {
                return null;
            }
        }
        public void setServerUrl(String value)
        {
            localSettings.Values[SERVER_URL] = value;
        }

        private readonly static String SERVER_NAME = "SERVER_NAME";
        public String getServerName()
        {
            try { 
                return (String)localSettings.Values[SERVER_NAME];
            }
            catch
            {
                return null;
            }
        }
        public void setServerName(String value)
        {
            localSettings.Values[SERVER_NAME] = value;
        }


    }
}
