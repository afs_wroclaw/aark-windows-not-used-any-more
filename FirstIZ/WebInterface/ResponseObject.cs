﻿using System.Runtime.Serialization;

namespace FirstIZ.WebInterface
{

    [DataContract]
    public class ResponseObject<T>
    {
        [DataMember]
        public string ResponseDetails { get; set; }
        [DataMember]
        public bool ResponseStatus { get; set; }
        [DataMember]
        public T ResponseData { get; set; }
    }

}
