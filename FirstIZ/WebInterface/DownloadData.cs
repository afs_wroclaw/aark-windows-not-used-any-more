﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FirstIZ.WebInterface
{
    [DataContract]
    class DownloadData
    {
        [DataMember(Order = 1)]
        public string AppVersion { get; set; }
        [DataMember(Order = 2)]
        public string AuthToken { get; set; }
        [DataMember(Order = 3)]
        public string LastSyncDate { get; set; }
    }
}
