﻿using System.Runtime.Serialization;
namespace FirstIZ.WebInterface
{
    [DataContract]
    public class StartRequest
    {
        [DataMember(Name = "AppVersion")]
        public string AppVersion { get; set; }
        [DataMember(Name = "AuthToken")]
        public string AuthToken { get; set; }
        [DataMember(Name = "DrivingCapacityID")]
        public long DrivingCapacityID { get; set; }
        [DataMember(Name = "IMEI")]
        public string IMEI { get; set; }
        [DataMember(Name = "RouteStartDate")]
        public string RouteStartDate { get; set; }
        [DataMember(Name = "TractionTypeID")]
        public long TractionTypeID { get; set; }
    }
}
