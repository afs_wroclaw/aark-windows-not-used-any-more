﻿using SQLite.Net.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FirstIZ.WebInterface
{
    [DataContract]
    class DownloadDataResp
    {


        [DataMember(Name = "DrivingCapacities")]
        public IList<DrivingCapacity> DrivingCapacities { get; set; }
        [DataMember(Name = "SyncDate")]
        public DateTime SyncDate { get; set; }
        [DataMember(Name = "TractionTypes")]
        public IList<TractionType> TractionTypes { get; set; }


        [DataContract]
        public class DrivingCapacity
        {
            [DataMember(Name = "ID")]
            [PrimaryKey]
            public int Id { get; set; }
            [DataMember(Name = "IsActive")]
            public bool IsActive { get; set; }
            [DataMember(Name = "Name")]
            public string Name { get; set; }

            public override string ToString() {
                return Name;
            }
        }

        [DataContract]
        public class TractionType
        {
            [DataMember(Name = "ID")]
            [PrimaryKey]
            public int Id { get; set; }
            [DataMember(Name = "IsActive")]
            public bool IsActive { get; set; }
            [DataMember(Name = "Name")]
            public string Name { get; set; }

            public override string ToString()
            {
                return Name;
            }
        }
        
    }
}
