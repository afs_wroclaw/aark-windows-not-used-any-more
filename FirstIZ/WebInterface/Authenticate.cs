﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FirstIZ.WebInterface
{
    [DataContract]
    class Authenticate
    {
        [DataMember(Order = 1)]
        public string AppVersion { get; set; }
        [DataMember(Order = 2)]
        public string Login { get; set; }
        [DataMember(Order = 3)]
        public string Password { get; set; }
    }
}
