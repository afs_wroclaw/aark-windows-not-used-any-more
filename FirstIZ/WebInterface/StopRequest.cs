﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace FirstIZ.WebInterface
{
    [DataContract]
    public class ObservedPoint
    {
        [DataMember(Name = "DateRecorded")]
        public string DateRecorded { get; set; }
        [DataMember(Name = "Latitude")]
        public double Latitude { get; set; }
        [DataMember(Name = "Longitude")]
        public double Longitude { get; set; }
        [DataMember(Name = "OrderNo")]
        public long OrderNo { get; set; }
        [DataMember(Name = "A")]
        public double Accuracy { get; set; }
    }

    [DataContract]
    public class StopRequest
    {
        [DataMember(Name = "AppVersion")]
        public string AppVersion { get; set; }
        [DataMember(Name = "AuthToken")]
        public string AuthToken { get; set; }
        [DataMember(Name = "ObservedPoints")]
        public IList<ObservedPoint> ObservedPoints { get; set; }
        [DataMember(Name = "RouteID")]
        public long RouteID { get; set; }
        [DataMember(Name = "RouteStopDate")]
        public string RouteStopDate { get; set; }
    }

}
