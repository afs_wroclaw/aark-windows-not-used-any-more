﻿

using System.Runtime.Serialization;

namespace FirstIZ.WebInterface
{
    [DataContract]
    class StartRouteResponseData
    {
        [DataMember(Name = "NewRouteId")]
        public int NewRouteId { get; set; }
    }
}
