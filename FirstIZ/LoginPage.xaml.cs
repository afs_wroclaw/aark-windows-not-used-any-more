﻿using FirstIZ.WebInterface;
using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Windows.Security.Cryptography;
using Windows.Security.Cryptography.Core;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace FirstIZ
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class LoginPage : Page
    {
        HttpClient httpClient;
        SettingsAdapter settingsAdapter;

        public LoginPage()
        {
            this.InitializeComponent();
            settingsAdapter = new SettingsAdapter();
            errorText.Visibility = Visibility.Collapsed;
            AppInfo.Text = ServerData.getWebServiceName();
            setViewState(true);
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            if (loginTextBox.Text.Length < 1 || passwordTextBox.Password.Length < 1) { 
                errorText.Visibility = Visibility.Visible;
                return;
            }else
                errorText.Visibility = Visibility.Collapsed;

            setViewState(false);
            authenticate();

        }

        private void setViewState(bool active)
        {
            if (active)
            {
                button.Visibility = Visibility.Visible;
                progrssRing.Visibility = Visibility.Collapsed;
                loginTextBox.IsEnabled = true;
                passwordTextBox.IsEnabled = true;
            }
            else
            {
                button.Visibility = Visibility.Collapsed;
                progrssRing.Visibility = Visibility.Visible;
                loginTextBox.IsEnabled = false;
                passwordTextBox.IsEnabled = false;
            }
        }

        private async void authenticate()
        {
            NotifyUser("");
            httpClient = new HttpClient();
            try
            {
                string resourceAddress = ServerData.getWebServiceUrl() + "AuthUser";
                string login = loginTextBox.Text;
                string password = computeMD5(passwordTextBox.Password);

                Authenticate data = new Authenticate { AppVersion = "Windows", Login = login, Password = password };
                string postBody = JsonSerializer(data);
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage wcfResponse = await httpClient.PostAsync(resourceAddress, new StringContent(postBody, Encoding.UTF8, "application/json"));
                await DisplayTextResult(wcfResponse, OutputField);
            }
            catch (HttpRequestException hre)
            {
                var messageDialog = new Windows.UI.Popups.MessageDialog("Cannot connect to server. Check your Internet connection");
                messageDialog.Commands.Add(new Windows.UI.Popups.UICommand("OK") { Id = 0 });
                await messageDialog.ShowAsync();
                //NotifyUser("Error:" + hre.Message);
            }
            catch (TaskCanceledException)
            {
                NotifyUser("Request canceled.");
            }
            catch (Exception ex)
            {
                NotifyUser(ex.Message);
            }
            finally
            {
                setViewState(true);
                if (httpClient != null)
                {
                    httpClient.Dispose();
                    httpClient = null;
                }
            }
        }


        /// <summary>
        /// Display Result which returns from WCF Service in "OutputField" Textbox
        /// </summary>
        /// <param name="response">Http response Message</param>
        /// <param name="output">Show result control</param>
        /// <returns></returns>
        private async Task DisplayTextResult(HttpResponseMessage response, TextBlock output)
        {
            string responJsonText = await response.Content.ReadAsStringAsync();
            ResponseObject responseObject = Deserialize<ResponseObject>(responJsonText);
            if(responseObject==null)
                output.Text = "Error";
            else if (responseObject.ResponseStatus)
            {
                //success
                settingsAdapter.setUserName(responseObject.ResponseData.UserName); ;
                settingsAdapter.setToken(responseObject.ResponseData.AuthToken);
                settingsAdapter.setLogged(true);
                Frame.Navigate(typeof(MainPage));
                
            }
            else
                output.Text = responseObject.ResponseDetails;
        }

        /// <summary>
        /// Serialize Person object to Json string
        /// </summary>
        /// <param name="objectToSerialize">Person object instance</param>
        /// <returns>return Json String</returns>
        private string JsonSerializer(Authenticate objectToSerialize)
        {
            if (objectToSerialize == null)
            {
                throw new ArgumentException("objectToSerialize must not be null");
            }
            MemoryStream ms = null;

            
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(objectToSerialize.GetType());
            ms = new MemoryStream();
            serializer.WriteObject(ms, objectToSerialize);
            
            ms.Seek(0, SeekOrigin.Begin);
            StreamReader sr = new StreamReader(ms);
            return sr.ReadToEnd();
        }
        

        public void NotifyUser(string message)
        {
            this.OutputField.Text = message;
        }

        private static T Deserialize<T>(string json)
        {
            var instance = Activator.CreateInstance<T>();
            using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(json)))
            {
                var serializer = new DataContractJsonSerializer(instance.GetType());
                return (T)serializer.ReadObject(ms);
            }
        }

        [DataContract]
        public class ResponseData
        {
            [DataMember]
            public string AuthToken { get; set; }
            [DataMember]
            public string UserName { get; set; }
        }

        [DataContract]
        public class ResponseObject
        {
            [DataMember]
            public string ResponseDetails { get; set; }
            [DataMember]
            public bool ResponseStatus { get; set; }
            [DataMember]
            public ResponseData ResponseData { get; set; }
        }

        private static string computeMD5(string str)
        {
            var alg = HashAlgorithmProvider.OpenAlgorithm(HashAlgorithmNames.Md5);
            IBuffer buff = CryptographicBuffer.ConvertStringToBinary(str, BinaryStringEncoding.Utf8);
            var hashed = alg.HashData(buff);
            var res = CryptographicBuffer.EncodeToHexString(hashed);
            return res;
        }

        private int hiddenButtonCounter=0;
        private void hiddenButton_Click(object sender, RoutedEventArgs e)
        {
            hiddenButtonCounter++;
            if (hiddenButtonCounter > 5)
            {
                hiddenButtonCounter = 0;
                ServerData.toggle();
                AppInfo.Text = ServerData.getWebServiceName();
            }
        }

        private async void ExportLogs(object sender, RoutedEventArgs e)
        {
            var messageDialog = new Windows.UI.Popups.MessageDialog("It will save app error logs in downloads folder.");
            messageDialog.Commands.Add(new Windows.UI.Popups.UICommand("Cancel") { Id = 0 });
            messageDialog.Commands.Add(new Windows.UI.Popups.UICommand("Continue") { Id = 1 });
            var result = await messageDialog.ShowAsync();
            if ((int)result.Id == 1)
            {
                ((App)App.Current).CopyErrorFileToDownolads();
                var successDialog = new Windows.UI.Popups.MessageDialog("Saved in Downloads folder");
                toggleMenu(sender, e);
                await successDialog.ShowAsync();
            }
            else
            {
                toggleMenu(sender, e);
            }
        }

        private void toggleMenu(object sender, RoutedEventArgs e)
        {
            if(sideMenu.Visibility == Visibility.Visible)
                sideMenu.Visibility = Visibility.Collapsed;
            else
                sideMenu.Visibility = Visibility.Visible;
        }
    }
}
